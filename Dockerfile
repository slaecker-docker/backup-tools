FROM balenalib/raspberrypi3-alpine

RUN [ "cross-build-start" ]

RUN apk add --no-cache rsync sqlite postgresql

ENV BK_SOURCE_DIR=/data/source
ENV BK_TARGET_DIR=/data/target

ADD backup.sh /usr/local/bin/backup
RUN chmod +x /usr/local/bin/backup

ENTRYPOINT ["/bin/sh"]
CMD ["/usr/local/bin/backup"]

RUN [ "cross-build-end" ]
