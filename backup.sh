#!/bin/sh

cd $BK_TARGET_DIR || (echo "Backup target dir not accessible"; exit 1)

## SQLITE DBS ##
if [ ! -z $SQLITE_DBS ]; then
    echo ">>> BACKUP - SQLITE - $(date +"%F %T")"
    for i in $SQLITE_DBS; do
        dbdir="$BK_SOURCE_DIR/$(dirname $i)"
        dbfile="$(basename $i)"
        echo "> $i"
        sqlite3 $dbdir/$dbfile ".backup $dbfile.sqlite.backup.$(date +%F_%H-%M-%S)" \
            && echo "    $dbfile.sqlite.backup.$(date +%F_%H-%M-%S) DONE" \
            || echo "    $dbfile.sqlite.backup.$(date +%F_%H-%M-%S) FAILED"
        sqlite3 $dbdir/$dbfile .dump > $dbfile.sqlite.dump.$(date +%F_%H-%M-%S) \
            && echo "    $dbfile.sqlite.dump.$(date +%F_%H-%M-%S) DONE" \
            || echo "    $dbfile.sqlite.dump.$(date +%F_%H-%M-%S) FAILED"
    done
else
    echo "No SQLite databases defined for backup."
fi

## POSTGRESQL DBS ##
if [ ! -z $POSTGRES_DBS ]; then
    echo ">>> BACKUP - POSTGRESQL - $(date +"%F %T")"
    if [ ! -z $PGHOST ] && [ ! -z $PGPORT ] && [ ! -z $PGUSER ] && [ ! -z $PGPASS ]; then
        if which pg_dump > /dev/null 2>&1; then
            echo "*:*:*:$PGUSER:$PGPASS" > ~/.pgpass
            chmod 600 ~/.pgpass
            for db in $POSTGRES_DBS; do
                echo "> $db"
                pg_dump $db > $db.pgsql.dump.$(date +%F_%H-%M-%S) \
                    && echo "    $db.pgsql.dump.$(date +%F_%H-%M-%S) DONE" \
                    || echo "    $db.pgsql.dump.$(date +%F_%H-%M-%S) FAILED"
            done
        else
            echo "/!\ pg_dump not found, can't perform PostgreSQL backups!"
        fi
    else
        echo "Missing variable definition:"
        echo "    PGHOST: $PGHOST"
        echo "    PGPORT: $PGPORT"
        echo "    PGUSER: $PGUSER"
        echo "    PGPASS: $PGPASS"
    fi
else
    echo "No PostgreSQL databases defined for backup."
fi

